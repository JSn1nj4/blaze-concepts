import 'meteor/reactive-var';
import './reactive-props.html';
import '../../components/status-message/status-message.js';

Template.App_reactiveProps.onCreated(function reactivePropsOnCreated() {
  this.messageVar = {};
  this.messageVar.first = new ReactiveVar('');
  this.messageVar.second = new ReactiveVar('');

  this.setMessage = (text, msgBox) => {
    this.messageVar[msgBox].set(text);
    return this;
  };
});

Template.App_reactiveProps.events({
  'submit .message-form'(e) {
    e.preventDefault();

    const target = e.target;
    const msgBox = target.msgBox.value;
    const text = target.text.value;

    Template.instance().setMessage(text, msgBox);
    target.reset();
  }
});

Template.App_reactiveProps.helpers({
  getMessageVar(msgVar) {
    return Template.instance().messageVar[msgVar];
  }
});
