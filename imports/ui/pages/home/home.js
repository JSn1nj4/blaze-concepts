import './home.html';
import '../../components/info/info.js';
import { simpleRoutes } from '../../../startup/client/routes.js';

Template.App_home.helpers({
  getRoutes() {
    return simpleRoutes;
  },
  routeIsHome(route) {
    return route.toLowerCase() === 'home';
  }
});
