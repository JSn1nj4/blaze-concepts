import './status-message.html';

Template.StatusMessage.onCreated(function statusMessageOnCreated() {
  this.messageVisibility = new ReactiveVar('hidden');
  this.message = this.data.messageVar;

  this.hideMessage = () => {
    this.messageVisibility.set('hidden');
    return this;
  };
  this.showMessage = () => {
    if( !this.message || this.message.get() === '') {
      return false;
    }

    this.messageVisibility.set('visible');
    setTimeout(this.hideMessage, 2000);
    return this;
  };

  this.autorun(() => {
    if(!this.message) {
      return false;
    }

    this.showMessage();
  });
});

Template.StatusMessage.helpers({
  messageVisibility() {
    return Template.instance().messageVisibility.get();
  },
  getMessage() {
    if(!Template.instance().message) {
      return false;
    }

    return Template.instance().message.get();
  }
});
