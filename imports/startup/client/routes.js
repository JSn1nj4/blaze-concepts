import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

// Import needed templates
import '../../ui/layouts/body/body.js';
import '../../ui/pages/home/home.js';
import '../../ui/pages/not-found/not-found.js';
import '../../ui/pages/reactive-props/reactive-props.js';

export let simpleRoutes = [
  'home',
  'reactive-props'
];

// Set up all routes in the app
simpleRoutes.map(route => {
  let capitalize = function(str) {
    return str.split('').map((letter, i) => i === 0 ? letter.toUpperCase() : letter ).join('');
  };

  let template = route.split('-').map((word, i) => i > 0 ? capitalize(word) : word ).join('');

  FlowRouter.route(`/${route !== 'home' ? route : ''}`, {
    name: `App.${route}`,
    action() {
      BlazeLayout.render('App_body', { main: `App_${template}`});
    }
  });
});

FlowRouter.notFound = {
  action() {
    BlazeLayout.render('App_body', { main: 'App_notFound' });
  },
};
